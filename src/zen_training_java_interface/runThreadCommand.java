package zen_training_java_interface;

public class RunThreadCommand implements Runnable, Command {

	@Override
	public void execute() {
		Thread thread = new Thread(new RunThreadCommand());
		thread.start();
	}

	@Override
	public void run() {
		for(int i = 0; i<5; i++) {
			System.out.println("runThreadCommand: i = " + i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
