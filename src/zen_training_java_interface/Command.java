package zen_training_java_interface;

public interface Command {
	public void execute();
}
