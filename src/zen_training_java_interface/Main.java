package zen_training_java_interface;

import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) {
		ArrayList<Command> commandStack = new ArrayList<>();
		commandStack.add(new PrintHWCommand());
		commandStack.add(new RunThreadCommand());
		commandStack.add(new RunThreadCommand());
		
		for(Command command: commandStack) {command.execute();}
	}
}
